import sqlite3
from flask import Flask, render_template, url_for, request, flash, redirect, abort
app = Flask(__name__)
app.config['SECRET_KEY'] = 'a245a7075ea794514446c234c863b31d'

def get_db_connection():
    conn = sqlite3.connect('database.db')
    conn.row_factory = sqlite3.Row
    return conn

def get_post(post_id):
    conn = get_db_connection()
    post = conn.execute('SELECT * FROM posts WHERE id = ?',
                        (post_id,)).fetchone()
    conn.close()
    if post is None:
        abort(404)
    return post

@app.route("/")
@app.route("/home")
def home():
    return render_template('index.html')

@app.route("/login")
def login():

    return render_template('login.html')

@app.route("/register")
def register():
    return render_template('register.html')

@app.route("/blog")
def blog():
    conn = get_db_connection()
    posts = conn.execute('SELECT * FROM posts').fetchall()
    conn.close()
    return render_template('blog.html', posts=posts)

@app.route('/create/', methods=('GET', 'POST'))
def create():
    if request.method == 'POST':
        author = request.form['author']
        title = request.form['title']
        content = request.form['content']

        if not author:
            flash('Author is required!')
        elif not title:
            flash('Title is required!')
        elif not content:
            flash('Content is required!')
        else:
            conn = get_db_connection()
            conn.execute('INSERT INTO posts (author, title, content) VALUES (?, ?, ?)',
                            (author, title, content))
            conn.commit()
            conn.close()
            return redirect(url_for('blog'))
        
    return render_template('create.html')

@app.route('/<int:id>/edit/', methods=('GET', 'POST'))
def edit(id):
    post = get_post(id)

    if request.method == 'POST':
        author = request.form['author']
        title = request.form['title']
        content = request.form['content']

        if not author:
            flash('Author is required!')

        elif not title:
            flash('Title is required!')
        
        elif not content:
            flash('Content is required!')

        else:
            conn = get_db_connection()
            conn.execute('UPDATE posts SET author = ?, title = ?, content = ?'
                         ' WHERE id = ?',
                         (author, title, content, id))
            conn.commit()
            conn.close()
            return redirect(url_for('blog'))

    return render_template('edit.html', post=post)

@app.route('/<int:id>/delete/', methods=('POST',))
def delete(id):
    post = get_post(id)
    conn = get_db_connection()
    conn.execute('DELETE FROM posts WHERE id = ?', (id,))
    conn.commit()
    conn.close()
    flash('"{}" was successfully deleted!'.format(post['title']))
    return redirect(url_for('blog'))

@app.route("/videolib")
def videoLibrary():
    return render_template('vidlib.html')

@app.route("/chestVideos")
def chestVideos():
    return render_template('chestVideos.html')

@app.route("/backVideos")
def backVideos():
    return render_template('backVideos.html')

@app.route("/shoulderVideos")
def shoulderVideos():
    return render_template('shoulderVideos.html')

@app.route("/armsVideos")
def armsVideos():
    return render_template('armsVideos.html')

@app.route("/absVideos")
def absVideos():
    return render_template('absVideos.html')

@app.route("/quadsVideos")
def quadsVideos():
    return render_template('quadsVideos.html')

@app.route("/hamstringVideos")
def hamstringVideos():
    return render_template('hamstringVideos.html')

@app.route("/calvesVideos")
def calvesVideos():
    return render_template('calvesVideos.html')

if __name__ == '__main__':
    app.run(debug=True)
