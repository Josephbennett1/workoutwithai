from operator import methodcaller
from flask import Flask, render_template, request, url_for, session, logging, redirect, flash
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.sql import text

from passlib.hash import sha256_crypt

engine = create_engine("mysql+pymysql://root:password123@localhost/register")
                    #(mysql+pymysql://username:password@localhost/databasename)
db=scoped_session(sessionmaker(bind=engine))
app = Flask(__name__)

#Home page route
@app.route('/')
def home():
    return render_template('home.html')

#Register page route
@app.route('/register', methods=["GET", "POST"])
def register():
    if request.method == "POST":
        name = request.form.get("name")
        username = request.form.get("username")
        password = request.form.get("password")
        confirm = request.form.get("confirm")
        secure_password = sha256_crypt.encrypt(str(password))

        if password == confirm:
            db.execute(text("INSERT INTO users(name, username, password) VALUES(:name, :username, :password)"),
                       {"name":name, "username":username, "password":secure_password})
            db.commit()
            flash("You are registered! please login.","success")
            return redirect(url_for('login'))
        else:
            flash("Password does not match","danger")
            return render_template("register.html")

    return render_template('register.html')

#Login page route
@app.route('/login', methods=["GET","POST"])
def login():
    if request.method == "POST":
        username = request.form.get("username")
        password = request.form.get("password")

        usernamedata = db.execute(text("SELECT username FROM users WHERE username=username"),
                                  {"username":username}).fetchone()
        passwordata = db.execute(text("SELECT password FROM users WHERE username=username"),
                                 {"username":username}).fetchone()

        if usernamedata is None:
            flash("No username","danger")
            return render_template("login.html")
        else:
            for passwor_data in passwordata:
                if sha256_crypt.verify(password,passwor_data):
                    session["log"] = True
                    flash("You have been logged in","success")
                    return redirect(url_for('home'))
                
                else:
                    flash("Username or password is incorrect","danger")
                    return render_template("login.html")
        
    return render_template('login.html')

#logout system
@app.route('/logout')
def logout():
    session.clear()
    flash("You have logged out", "success")
    return redirect(url_for('home'))


if __name__ == '__main__':
    app.secret_key="1234567wwacoding"
    app.run(host="0.0.0.0", port=5500, debug=True)
