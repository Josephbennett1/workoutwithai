Workout With AI

file - WorkoutWithAIapp contains:
        Python/Flask web application, to run app.py:
            python3 app.py in terminal

        This file includes a working demonstration of the blog feature using sqlite3 that can be used by PTs and other fitness professionals to
        post content for their clients. This file also includes and example of a working video library, accessable to all users so they are
        able to access video demonstrations of exercises if help is needed with a certain movement. A Login and Register template is 
        provided in this document. NOTE. login system does not work on this application.
        Specific packages are required to run this application - please see requirements.txt for more information.

file - login_system contains:
        Python/Flask application, to run app.py 
            python3 app.py in terminal

        This file includes a working demonstration of a Register and Login system using SQLalchemy. 
        Specific packages are required to run this application - please see requirements.txt for more information.

        to set up the database:
        open mysql on terminal
        >mysql -u root -p
        >password123
        >CREATE DATABASE register;
        >USE register;
        >CREATE TABLE users(id SERIAL PRIMARY KEY, name VARCHAR(50) NOT NULL, username VARCHAR(50) NOT NULL, password VARCHAR(300) NOT NULL);




file - WorkoutGeneratorML contains:
       Jupyter Notebook for workout generator machine learning algorithm attempt
